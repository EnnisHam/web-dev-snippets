type SaveSelectProps = {
  saveContent: string;
  disabled?: boolean;
};

/**
 * feature not implemented outside of chrome currently
 * @param props some stuff
 * @returns a button that downloads its content to the file location
 */
export const SaveSelectButton = (props: SaveSelectProps) => {
  const { saveContent } = props;
  const disabled = props.disabled || window.showSaveFilePicker === undefined;

  const clickToSave = async (content: string) => {
    const handler = await window.showSaveFilePicker();
    const stream = await handler.createWritable();

    try {
      stream.write(content);
    } catch {
      console.error('some thing fucked up');
    } finally {
      stream.close();
    }
  }

  return (
    <div>
      <button onClick={() => clickToSave(saveContent)} disabled={disabled}>
        Click to Save
      </button>
    </div>
  );
};